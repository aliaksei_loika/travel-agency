CREATE TABLE country (
  id      integer primary key NOT NULL identity,
  version integer default 0,
  name    varchar(100)
);

CREATE TABLE hotel (
  id         integer primary key  NOT NULL identity,
  version    integer default 0,
  name       varchar(100)         NOT NULL,
  stars      integer              NOT NULL,
  country_id integer              NOT NULL,
  phone      varchar(30)
);

CREATE TABLE tourtype (
  id   integer primary key NOT NULL identity,
  name varchar(30)
);

CREATE TABLE users (
  id      integer primary key      NOT NULL identity,
  version integer default 0,
  login   varchar(30)              NOT NULL,
  pass    varchar(30)              NOT NULL
);

CREATE TABLE tour (
  id          integer primary key  NOT NULL identity,
  version     integer default 0,
  photo       BLOB,
  start_date  date                 NOT NULL,
  duration    integer,
  hotel_id    integer,
  tourtype_id integer,
  country_id  integer,
  description varchar(1000),
  cost        integer              NOT NULL

);

CREATE INDEX idx_tour_tourtype_id
  ON tour (tourtype_id);

CREATE INDEX idx_tour_country_id
  ON tour (country_id);

CREATE TABLE tour_user (
  tour_id integer NOT NULL,
  user_id integer NOT NULL
);

CREATE INDEX idx_tour_user_tour_id
  ON tour_user (tour_id);

CREATE INDEX idx_tour_user_user_id
  ON tour_user (user_id);

CREATE TABLE review (
  id      integer primary key NOT NULL identity,
  version integer default 0,
  content varchar(1000)       NOT NULL,
  tour_id integer,
  user_id integer
);

CREATE INDEX idx_review_tour_id
  ON review (tour_id);

CREATE INDEX idx_review_user_id
  ON review (user_id);

ALTER TABLE hotel
  ADD CONSTRAINT fk_hotel_country FOREIGN KEY (country_id) REFERENCES country (id);

ALTER TABLE review
  ADD CONSTRAINT fk_review_tour FOREIGN KEY (tour_id) REFERENCES tour (id);

ALTER TABLE review
  ADD CONSTRAINT fk_review_user FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE tour
  ADD CONSTRAINT fk_tour_hotel FOREIGN KEY (hotel_id) REFERENCES hotel (id);

ALTER TABLE tour
  ADD CONSTRAINT fk_tour_tourtype FOREIGN KEY (tourtype_id) REFERENCES tourtype (id);

ALTER TABLE tour
  ADD CONSTRAINT fk_tour_country FOREIGN KEY (country_id) REFERENCES country (id);

ALTER TABLE tour_user
  ADD CONSTRAINT fk_tour_user_tour FOREIGN KEY (tour_id) REFERENCES tour (id);

ALTER TABLE tour_user
  ADD CONSTRAINT fk_tour_user_user FOREIGN KEY (user_id) REFERENCES users (id);