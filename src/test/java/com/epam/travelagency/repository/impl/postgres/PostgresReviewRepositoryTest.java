package com.epam.travelagency.repository.impl.postgres;

import com.epam.travelagency.configuration.AppTestConfig;
import com.epam.travelagency.domain.Review;
import com.epam.travelagency.domain.Tour;
import com.epam.travelagency.domain.User;
import com.epam.travelagency.repository.ReviewRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@ContextConfiguration(classes = {AppTestConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("postgresql")
public class PostgresReviewRepositoryTest {

    @Autowired
    private ReviewRepository reviewRepository;

    @Test
    public void takeAll() {
        Assert.assertNotNull(reviewRepository.takeAll());
    }

    @Test
    public void create() {
        Tour tour = new Tour();
        tour.setId(2);
        User user = new User();
        user.setId(1);
        Review review = new Review();
        review.setTour(tour);
        review.setUser(user);
        review.setContent("Bla-bla-bla");
        Assert.assertTrue(reviewRepository.create(review));
    }

    @Test
    public void take() {
        Assert.assertNotNull(reviewRepository.take(2L));
    }

    @Test
    public void update() {
        Review review = reviewRepository.take(2L).get();
        review.setContent("Tra-la-la");
        Assert.assertTrue(reviewRepository.update(review));
    }

    @Test
    public void delete() {
        Review review = new Review();
        review.setId(1L);
        Assert.assertTrue(reviewRepository.delete(review));
    }
}