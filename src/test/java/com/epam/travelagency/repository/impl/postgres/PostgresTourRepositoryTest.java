package com.epam.travelagency.repository.impl.postgres;

import com.epam.travelagency.configuration.AppTestConfig;
import com.epam.travelagency.domain.Country;
import com.epam.travelagency.domain.Hotel;
import com.epam.travelagency.domain.Tour;
import com.epam.travelagency.repository.TourRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.util.Optional;

@ContextConfiguration(classes = {AppTestConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("postgresql")
public class PostgresTourRepositoryTest {

    @Autowired
    private TourRepository tourRepository;

    @Test
    public void takeAll() {
        Assert.assertNotNull(tourRepository.takeAll());
    }

    @Test
    public void create() {
        Hotel hotel = new Hotel();
        hotel.setId(2L);
        Country country = new Country();
        country.setId(1L);

        Tour tour = new Tour();
        tour.setDate(LocalDate.of(2018, 1, 1));
        tour.setDuration(14);
        tour.setHotel(hotel);
        tour.setTourType(Tour.TourType.NORMAL);
        tour.setCountry(country);
        tour.setCost(1000);

        Assert.assertTrue(tourRepository.create(tour));
    }

    @Test
    public void take() {
        Assert.assertTrue(tourRepository.take(2L).isPresent());
    }

    @Test
    public void update() {
        Optional<Tour> tourOptional = tourRepository.take(2L);

        Tour tour = tourOptional.get();
        tour.setDescription("bla-bla-bla");

        Assert.assertTrue(tourRepository.update(tour));

    }

    @Test
    public void delete() {
        Tour tour = new Tour();
        tour.setId(4L);
        Assert.assertTrue(tourRepository.delete(tour));
    }
}