package com.epam.travelagency.repository.impl.jpa;

import com.epam.travelagency.configuration.AppTestConfig;
import com.epam.travelagency.domain.Country;
import com.epam.travelagency.domain.Hotel;
import com.epam.travelagency.repository.HotelRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration(classes = {AppTestConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("jpa")
@Transactional
public class HibernateHotelRepositoryTest {

    @Autowired
    private HotelRepository hotelRepository;

    @Test
    public void takeAll() {
        Assert.assertNotNull(hotelRepository.takeAll());
    }

    @Test
    public void create() {
        Country country = new Country();
        country.setId(1L);
        Hotel hotel = new Hotel();
        hotel.setName("Resort");
        hotel.setPhone("777");
        hotel.setCountry(country);
        hotel.setStars(2);
        Assert.assertTrue(hotelRepository.create(hotel));
    }

    @Test
    public void take() {
        Assert.assertNotNull(hotelRepository.take(1L));
    }

    @Test
    public void update() {
        Hotel hotel = hotelRepository.take(2).get();
        hotel.setStars(1);
        Assert.assertTrue(hotelRepository.update(hotel));

    }

    @Test
    public void delete() {
        Hotel hotel = new Hotel();
        hotel.setId(5L);
        Assert.assertTrue(hotelRepository.delete(hotel));
    }
}