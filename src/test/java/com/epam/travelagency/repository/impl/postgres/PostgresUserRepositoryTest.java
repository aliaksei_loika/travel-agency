package com.epam.travelagency.repository.impl.postgres;

import com.epam.travelagency.configuration.AppTestConfig;
import com.epam.travelagency.domain.User;
import com.epam.travelagency.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

@ContextConfiguration(classes = {AppTestConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("postgresql")
public class PostgresUserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void takeAll() {
        Assert.assertNotNull(userRepository.takeAll());
    }

    @Test
    public void create() {
        User user = new User();
        user.setLogin("login");
        user.setPassword("password");
        Assert.assertTrue(userRepository.create(user));
    }

    @Test
    public void take() {
        Assert.assertTrue(userRepository.take(2L).isPresent());
    }

    @Test
    public void update() {
        Optional<User> userOptional = userRepository.take(3L);

        User user = userOptional.get();
        user.setPassword("bla-bla-bla");

        Assert.assertTrue(userRepository.update(user));
    }

    @Test
    public void delete() {
        User user = new User();
        user.setId(4L);
        Assert.assertTrue(userRepository.delete(user));
    }
}