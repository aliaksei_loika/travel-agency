package com.epam.travelagency.repository.impl.postgres;

import com.epam.travelagency.configuration.AppTestConfig;
import com.epam.travelagency.domain.Country;
import com.epam.travelagency.repository.CountryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

@ContextConfiguration(classes = {AppTestConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("postgresql")
public class PostgresCountryRepositoryTest {

    @Autowired
    private CountryRepository countryRepository;

    @Test
    public void takeAll() {
        Assert.assertNotNull(countryRepository.takeAll());
    }

    @Test
    public void create() {
        Country country = new Country();
        country.setName("USA");
        Assert.assertTrue(countryRepository.create(country));
    }

    @Test
    public void take() {
        Assert.assertNotNull(countryRepository.take(1L));
    }

    @Test
    public void update() {
        Optional<Country> c1 = countryRepository.take(1L);
        c1.get().setName("TestEx");
        Assert.assertTrue(countryRepository.update(c1.get()));

    }

    @Test
    public void delete() {
        Country country = new Country();
        country.setId(4L);
        Assert.assertTrue(countryRepository.delete(country));
    }
}