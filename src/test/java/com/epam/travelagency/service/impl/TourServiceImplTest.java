package com.epam.travelagency.service.impl;

import com.epam.travelagency.domain.Tour;
import com.epam.travelagency.repository.TourRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class TourServiceImplTest {
    @Mock
    private Tour review;
    @Mock
    private TourRepository tourRepository;
    @InjectMocks
    private TourServiceImpl tourService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void takeAll() {
        tourService.takeAll();
        Mockito.verify(tourRepository).takeAll();
    }

    @Test
    public void create(){
        tourService.create(review);
        Mockito.verify(tourRepository).create(review);
    }

    @Test
    public void take(){
        tourService.take(review.getId());
        Mockito.verify(tourRepository).take(review.getId());
    }

    @Test
    public void update(){
        tourService.update(review);
        Mockito.verify(tourRepository).update(review);
    }

    @Test
    public void delete(){
        tourService.delete(review);
        Mockito.verify(tourRepository).delete(review);
    }

}