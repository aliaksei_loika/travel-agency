package com.epam.travelagency.service.impl;

import com.epam.travelagency.domain.Hotel;
import com.epam.travelagency.repository.HotelRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class HotelServiceImplTest {
    @Mock
    private Hotel hotel;
    @Mock
    private HotelRepository hotelRepository;
    @InjectMocks
    private HotelServiceImpl hotelService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void takeAll() {
        hotelService.takeAll();
        Mockito.verify(hotelRepository).takeAll();
    }

    @Test
    public void create(){
        hotelService.create(hotel);
        Mockito.verify(hotelRepository).create(hotel);
    }

    @Test
    public void take(){
        hotelService.take(hotel.getId());
        Mockito.verify(hotelRepository).take(hotel.getId());
    }

    @Test
    public void update(){
        hotelService.update(hotel);
        Mockito.verify(hotelRepository).update(hotel);
    }

    @Test
    public void delete(){
        hotelService.delete(hotel);
        Mockito.verify(hotelRepository).delete(hotel);
    }

}