package com.epam.travelagency.service.impl;

import com.epam.travelagency.domain.User;
import com.epam.travelagency.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class UserServiceImplTest {
    @Mock
    private User user;
    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private UserServiceImpl userService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void takeAll() {
        userService.takeAll();
        Mockito.verify(userRepository).takeAll();
    }

    @Test
    public void create(){
        userService.create(user);
        Mockito.verify(userRepository).create(user);
    }

    @Test
    public void take(){
        userService.take(user.getId());
        Mockito.verify(userRepository).take(user.getId());
    }

    @Test
    public void update(){
        userService.update(user);
        Mockito.verify(userRepository).update(user);
    }

    @Test
    public void delete(){
        userService.delete(user);
        Mockito.verify(userRepository).delete(user);
    }

}