package com.epam.travelagency.service.impl;

import com.epam.travelagency.domain.Review;
import com.epam.travelagency.repository.ReviewRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class ReviewServiceImplTest {
    @Mock
    private Review review;
    @Mock
    private ReviewRepository reviewRepository;
    @InjectMocks
    private ReviewServiceImpl reviewService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void takeAll() {
        reviewService.takeAll();
        Mockito.verify(reviewRepository).takeAll();
    }

    @Test
    public void create(){
        reviewService.create(review);
        Mockito.verify(reviewRepository).create(review);
    }

    @Test
    public void take(){
        reviewService.take(review.getId());
        Mockito.verify(reviewRepository).take(review.getId());
    }

    @Test
    public void update(){
        reviewService.update(review);
        Mockito.verify(reviewRepository).update(review);
    }

    @Test
    public void delete(){
        reviewService.delete(review);
        Mockito.verify(reviewRepository).delete(review);
    }

}