package com.epam.travelagency.service.impl;

import com.epam.travelagency.domain.Country;
import com.epam.travelagency.repository.CountryRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class CountryServiceImplTest {
    @Mock
    private Country country;
    @Mock
    private CountryRepository countryRepository;
    @InjectMocks
    private CountryServiceImpl countryService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void takeAll() {
        countryService.takeAll();
        Mockito.verify(countryRepository).takeAll();
    }

    @Test
    public void create(){
        countryService.create(country);
        Mockito.verify(countryRepository).create(country);
    }

    @Test
    public void take(){
        countryService.take(country.getId());
        Mockito.verify(countryRepository).take(country.getId());
    }

    @Test
    public void update(){
        countryService.update(country);
        Mockito.verify(countryRepository).update(country);
    }

    @Test
    public void delete(){
        countryService.delete(country);
        Mockito.verify(countryRepository).delete(country);
    }

}