package com.epam.travelagency.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class AppTestConfig extends AppConfig {

    @Override
    @Bean
    public DataSource dataSource(){
        EmbeddedDatabaseBuilder embeddedDatabaseBuilder = new EmbeddedDatabaseBuilder();
        embeddedDatabaseBuilder.setType(EmbeddedDatabaseType.HSQL);
        embeddedDatabaseBuilder.generateUniqueName(true);
        embeddedDatabaseBuilder.setScriptEncoding("UTF-8");
        embeddedDatabaseBuilder.addScript("classpath:hsql/hsql-schema.sql");
        embeddedDatabaseBuilder.addScript("classpath:hsql/hsql-init-data.sql");
        return embeddedDatabaseBuilder.build();
    }

}
