package com.epam.travelagency.service;

import com.epam.travelagency.domain.Entity;

import java.util.List;
import java.util.Optional;

public interface Service <T extends Entity> {
    /**
     * Get all entities
     *
     * @return list the list of entities
     */
    List<T> takeAll();

    /**
     * Create new entity
     *
     * @param entity
     * @return result the boolean
     */
    boolean create(T entity);

    /**
     * Get entity by id
     *
     * @param id the id
     * @return entity the entity
     */
    Optional<T> take(long id);

    /**
     * Update entity
     *
     * @param entity the entity
     * @return result the boolean
     */
    boolean update(T entity);

    /**
     * Delete entity
     *
     * @param entity the entity
     * @return result the boolean
     */
    boolean delete(T entity);
}
