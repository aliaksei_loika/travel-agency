package com.epam.travelagency.service.impl;

import com.epam.travelagency.domain.Country;
import com.epam.travelagency.repository.CountryRepository;
import com.epam.travelagency.service.CountryService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service("countryService")
@Transactional
public class CountryServiceImpl implements CountryService {
    private static final Logger LOGGER = LogManager.getLogger();

    @Autowired
    private CountryRepository repository;

    public void setRepository(CountryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Country> takeAll() {
        return repository.takeAll();
    }

    @Override
    public boolean create(Country entity) {
        boolean result = repository.create(entity);
        LOGGER.log(Level.INFO, "Create new entity: " + result);
        return result;
    }

    @Override
    public Optional<Country> take(long id) {
        return repository.take(id);
    }

    @Override
    public boolean update(Country entity) {
        boolean result = repository.update(entity);
        LOGGER.log(Level.INFO, "Update new entity: " + result);
        return result;
    }

    @Override
    public boolean delete(Country entity) {
        boolean result = repository.delete(entity);
        LOGGER.log(Level.INFO, "Delete new entity: " + result);
        return result;
    }
}
