package com.epam.travelagency.service.impl;

import com.epam.travelagency.domain.Tour;
import com.epam.travelagency.repository.TourRepository;
import com.epam.travelagency.service.TourService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/***
 Author: Aliaksei Loika
 Date: 26.03.2018
 ***/
@Service("tourService")
@Transactional
public class TourServiceImpl implements TourService {
    private static final Logger LOGGER = LogManager.getLogger();

    @Autowired
    private TourRepository repository;

    public void setRepository(TourRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Tour> takeAll() {
        return repository.takeAll();
    }

    @Override
    public boolean create(Tour entity) {
        boolean result = repository.create(entity);
        LOGGER.log(Level.INFO, "Create new entity: " + result);
        return result;
    }

    @Override
    public Optional<Tour> take(long id) {
        return repository.take(id);
    }

    @Override
    public boolean update(Tour entity) {
        boolean result = repository.update(entity);
        LOGGER.log(Level.INFO, "Update new entity: " + result);
        return result;
    }

    @Override
    public boolean delete(Tour entity) {
        boolean result = repository.delete(entity);
        LOGGER.log(Level.INFO, "Delete new entity: " + result);
        return result;
    }


}
