package com.epam.travelagency.service.impl;

import com.epam.travelagency.domain.Hotel;
import com.epam.travelagency.repository.HotelRepository;
import com.epam.travelagency.service.HotelService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/***
 Author: Aliaksei Loika
 Date: 26.03.2018
 ***/
@Service("hotelService")
@Transactional
public class HotelServiceImpl implements HotelService {
    private static final Logger LOGGER = LogManager.getLogger();

    @Autowired
    private HotelRepository repository;

    public void setRepository(HotelRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Hotel> takeAll() {
        return repository.takeAll();
    }

    @Override
    public boolean create(Hotel entity) {
        boolean result = repository.create(entity);
        LOGGER.log(Level.INFO, "Create new entity: " + result);
        return result;
    }

    @Override
    public Optional<Hotel> take(long id) {
        return repository.take(id);
    }

    @Override
    public boolean update(Hotel entity) {
        boolean result = repository.update(entity);
        LOGGER.log(Level.INFO, "Update new entity: " + result);
        return result;
    }

    @Override
    public boolean delete(Hotel entity) {
        boolean result = repository.delete(entity);
        LOGGER.log(Level.INFO, "Delete new entity: " + result);
        return result;
    }

}
