package com.epam.travelagency.service.impl;

import com.epam.travelagency.domain.User;
import com.epam.travelagency.repository.UserRepository;
import com.epam.travelagency.service.UserService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/***
 Author: Aliaksei Loika
 Date: 26.03.2018
 ***/
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
    private static final Logger LOGGER = LogManager.getLogger();

    @Autowired
    private UserRepository repository;

    public void setRepository(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<User> takeAll() {
        return repository.takeAll();
    }

    @Override
    public boolean create(User entity) {
        boolean result = repository.create(entity);
        LOGGER.log(Level.INFO, "Create new entity: " + result);
        return result;
    }

    @Override
    public Optional<User> take(long id) {
        return repository.take(id);
    }

    @Override
    public boolean update(User entity) {
        boolean result = repository.update(entity);
        LOGGER.log(Level.INFO, "Update new entity: " + result);
        return result;
    }

    @Override
    public boolean delete(User entity) {
        boolean result = repository.delete(entity);
        LOGGER.log(Level.INFO, "Delete new entity: " + result);
        return result;
    }
}
