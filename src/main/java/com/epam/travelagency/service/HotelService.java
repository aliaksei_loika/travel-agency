package com.epam.travelagency.service;

import com.epam.travelagency.domain.Hotel;

public interface HotelService extends Service<Hotel> {
}
