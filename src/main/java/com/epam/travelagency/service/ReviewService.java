package com.epam.travelagency.service;

import com.epam.travelagency.domain.Review;

public interface ReviewService extends Service<Review> {
}
