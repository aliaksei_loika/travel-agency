package com.epam.travelagency.service;

import com.epam.travelagency.domain.Tour;

public interface TourService extends Service<Tour> {
}
