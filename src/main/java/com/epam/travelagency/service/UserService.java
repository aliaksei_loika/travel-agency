package com.epam.travelagency.service;

import com.epam.travelagency.domain.User;

public interface UserService extends Service<User> {
}
