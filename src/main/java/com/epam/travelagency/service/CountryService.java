package com.epam.travelagency.service;

import com.epam.travelagency.domain.Country;

public interface CountryService extends Service<Country> {
}
