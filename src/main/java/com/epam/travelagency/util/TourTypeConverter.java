package com.epam.travelagency.util;


import com.epam.travelagency.domain.Tour;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TourTypeConverter implements AttributeConverter<Tour.TourType, Long> {

    public Long convertToDatabaseColumn(Tour.TourType value) {
        if (value == null) {
            return null;
        }
        return value.getId();
    }

    public Tour.TourType convertToEntityAttribute(Long value) {
        if (value == null) {
            return null;
        }

        return Tour.getTourType(value);
    }
}