package com.epam.travelagency.repository;

import com.epam.travelagency.domain.Review;

public interface ReviewRepository extends Repository<Review> {
}
