package com.epam.travelagency.repository;

import com.epam.travelagency.domain.User;

public interface UserRepository extends Repository<User> {
}
