package com.epam.travelagency.repository.impl.postgres.rowmapper;

import com.epam.travelagency.domain.Country;
import com.epam.travelagency.domain.Hotel;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component("hotelRowMapper")
public class HotelRowMapper implements RowMapper<Hotel> {
    @Override
    public Hotel mapRow(ResultSet resultSet, int i) throws SQLException {
        Hotel hotel = new Hotel();

        hotel.setId(resultSet.getLong("id"));
        hotel.setName(resultSet.getString("name"));
        hotel.setPhone(resultSet.getString("phone"));
        hotel.setStars(resultSet.getInt("stars"));

        Country country = new Country();
        country.setId(resultSet.getLong("id"));
        hotel.setCountry(country);
        return hotel;
    }
}
