package com.epam.travelagency.repository.impl.postgres;

import com.epam.travelagency.domain.Country;
import com.epam.travelagency.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository("countryRepository")
@Profile("postgresql")
public class PostgresCountryRepository implements CountryRepository {
    private static final String SQL_TAKE_ALL_COUNTRIES = "SELECT * FROM country";
    private static final String SQL_CREATE_NEW_COUNTRY = "INSERT INTO country(name) VALUES (?)";
    private static final String SQL_TAKE_COUNTRY_BY_ID = "SELECT * FROM country WHERE id = ?";
    private static final String SQL_UPDATE_COUNTRY = "UPDATE country SET name=? WHERE id=?";
    private static final String SQL_DELETE_COUNTRY = "DELETE FROM country WHERE id=?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("countryRowMapper")
    private RowMapper<Country> rowMapper;

    public PostgresCountryRepository(JdbcTemplate jdbcTemplate, RowMapper<Country> rowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = rowMapper;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setRowMapper(RowMapper<Country> rowMapper) {
        this.rowMapper = rowMapper;
    }

    @Override
    public List<Country> takeAll() {
        return jdbcTemplate.query(SQL_TAKE_ALL_COUNTRIES, rowMapper);
    }

    @Override
    public boolean create(Country entity) {
        return jdbcTemplate.update(SQL_CREATE_NEW_COUNTRY, entity.getName()) > 0;
    }

    @Override
    public Optional<Country> take(long id) {

        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_TAKE_COUNTRY_BY_ID,
                    new Object[]{id},
                    rowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean update(Country entity) {
        return jdbcTemplate.update(SQL_UPDATE_COUNTRY, entity.getName(), entity.getId()) > 0;
    }

    @Override
    public boolean delete(Country entity) {
        return jdbcTemplate.update(SQL_DELETE_COUNTRY, entity.getId()) > 0;
    }
}
