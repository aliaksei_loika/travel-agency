package com.epam.travelagency.repository.impl.jpa;

import com.epam.travelagency.domain.Hotel;
import com.epam.travelagency.repository.HotelRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository("hotelRepository")
@Profile("jpa")
public class HibernateHotelRepository implements HotelRepository {

    private static final String NATIVE_HOTEL_UPDATE_QUERY = "UPDATE hotel SET name = ?, phone = ?, stars = ?, " +
            "country_id =? WHERE id = ?";
    private static final String NAME_OF_NAMED_QUERY = "deleteHotelById";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Hotel> takeAll() {
        CriteriaQuery<Hotel> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(Hotel.class);
        Root<Hotel> root = criteriaQuery.from(Hotel.class);
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean create(Hotel entity) {
        entityManager.joinTransaction();
        entityManager.persist(entity);
        return true;
    }

    @Override
    public Optional<Hotel> take(long id) {
        return Optional.ofNullable(entityManager.find(Hotel.class, id));
    }

    @Override
    public boolean update(Hotel entity) {
        entityManager.joinTransaction();
        Query query = entityManager.createNativeQuery(NATIVE_HOTEL_UPDATE_QUERY, Hotel.class);
        query.setParameter(1, entity.getName());
        query.setParameter(2, entity.getPhone());
        query.setParameter(3, entity.getStars());
        query.setParameter(4, entity.getCountry().getId());
        query.setParameter(5, entity.getId());
        return query.executeUpdate() > 0;
    }

    @Override
    public boolean delete(Hotel entity) {
        entityManager.joinTransaction();
        Query query = entityManager.createNamedQuery(NAME_OF_NAMED_QUERY).setParameter("id", entity.getId());
        return query.executeUpdate() > 0;
    }
}
