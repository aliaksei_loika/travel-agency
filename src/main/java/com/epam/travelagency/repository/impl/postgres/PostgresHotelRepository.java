package com.epam.travelagency.repository.impl.postgres;

import com.epam.travelagency.domain.Hotel;
import com.epam.travelagency.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository("hotelRepository")
@Profile("postgresql")
public class PostgresHotelRepository implements HotelRepository {
    private static final String SQL_TAKE_ALL_HOTELS = "SELECT * FROM hotel";
    private static final String SQL_CREATE_NEW_HOTEL = "INSERT INTO hotel(name, phone, stars, country_id) VALUES (?, ?, ?, ?)";
    private static final String SQL_TAKE_HOTEL_BY_ID = "SELECT * FROM hotel WHERE id = ?";
    private static final String SQL_UPDATE_HOTEL = "UPDATE hotel SET name=?, phone=?, stars=?, country_id=? WHERE id=?";
    private static final String SQL_DELETE_HOTEL = "DELETE FROM hotel WHERE id=?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("hotelRowMapper")
    private RowMapper<Hotel> rowMapper;

    public PostgresHotelRepository(JdbcTemplate jdbcTemplate, RowMapper<Hotel> rowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = rowMapper;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setRowMapper(RowMapper<Hotel> rowMapper) {
        this.rowMapper = rowMapper;
    }

    @Override
    public List<Hotel> takeAll() {
        return jdbcTemplate.query(SQL_TAKE_ALL_HOTELS, rowMapper);
    }

    @Override
    public boolean create(Hotel entity) {
        return jdbcTemplate.update(SQL_CREATE_NEW_HOTEL, entity.getName(), entity.getPhone(), entity.getStars(), entity.getCountry().getId()) > 0;
    }

    @Override
    public Optional<Hotel> take(long id) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_TAKE_HOTEL_BY_ID,
                    new Object[]{id},
                    rowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean update(Hotel entity) {
        return jdbcTemplate.update(SQL_UPDATE_HOTEL, entity.getName(), entity.getPhone(), entity.getStars(), entity.getCountry().getId(), entity.getId()) > 0;
    }

    @Override
    public boolean delete(Hotel entity) {
        return jdbcTemplate.update(SQL_DELETE_HOTEL, entity.getId()) > 0;
    }
}
