package com.epam.travelagency.repository.impl.postgres;

import com.epam.travelagency.domain.Tour;
import com.epam.travelagency.repository.TourRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Repository("tourRepository")
@Profile("postgresql")
public class PostgresTourRepository implements TourRepository {
    private static final String SQL_TAKE_ALL_TOURS = "SELECT * FROM tour";
    private static final String SQL_CREATE_NEW_TOUR = "INSERT INTO tour(photo, start_date, duration, hotel_id, tourtype_id, " +
            "country_id, description, cost) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_TAKE_TOUR_BY_ID = "SELECT * FROM tour WHERE id = ?";
    private static final String SQL_UPDATE_TOUR = "UPDATE tour SET photo=?, start_date=?, duration=?, hotel_id=?, tourtype_id=?, " +
            "country_id=?, description=?, cost=? WHERE id=?";
    private static final String SQL_DELETE_TOUR = "DELETE FROM tour WHERE id=?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("tourRowMapper")
    private RowMapper<Tour> rowMapper;

    public PostgresTourRepository(JdbcTemplate jdbcTemplate, RowMapper<Tour> rowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = rowMapper;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setRowMapper(RowMapper<Tour> rowMapper) {
        this.rowMapper = rowMapper;
    }

    @Override
    public List<Tour> takeAll() {
        return jdbcTemplate.query(SQL_TAKE_ALL_TOURS, rowMapper);
    }

    @Override
    public boolean create(Tour entity) {
        return jdbcTemplate.update(SQL_CREATE_NEW_TOUR, entity.getPhoto(), Date.valueOf(entity.getDate()),
                entity.getDuration(), entity.getHotel().getId(), entity.getTourType().ordinal() + 1,
                entity.getCountry().getId(), entity.getDescription(), entity.getCost()) > 0;
    }

    @Override
    public Optional<Tour> take(long id) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_TAKE_TOUR_BY_ID, new Object[]{id}, rowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean update(Tour entity) {
        return jdbcTemplate.update(SQL_UPDATE_TOUR, entity.getPhoto(), Date.valueOf(entity.getDate()),
                entity.getDuration(), entity.getHotel().getId(), entity.getTourType().ordinal() + 1,
                entity.getCountry().getId(), entity.getDescription(), entity.getCost(), entity.getId()) > 0;
    }

    @Override
    public boolean delete(Tour entity) {
        return jdbcTemplate.update(SQL_DELETE_TOUR, entity.getId()) > 0;
    }


}
