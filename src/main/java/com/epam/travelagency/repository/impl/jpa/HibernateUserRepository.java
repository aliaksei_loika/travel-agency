package com.epam.travelagency.repository.impl.jpa;

import com.epam.travelagency.domain.User;
import com.epam.travelagency.repository.UserRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository("userRepository")
@Profile("jpa")
public class HibernateUserRepository implements UserRepository {

    private static final String NAME_OF_NAMED_QUERY = "deleteUserById";

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public List<User> takeAll() {
        CriteriaQuery<User> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(User.class);
        Root<User> root = criteriaQuery.from(User.class);
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean create(User entity) {
        entityManager.joinTransaction();
        entityManager.persist(entity);
        return true;
    }

    @Override
    public Optional<User> take(long id) {
        return Optional.ofNullable(entityManager.find(User.class, id));
    }

    @Override
    public boolean update(User entity) {
        entityManager.joinTransaction();
        return entityManager.merge(entity) != null;
    }

    @Override
    public boolean delete(User entity) {
        entityManager.joinTransaction();
        Query query = entityManager.createNamedQuery(NAME_OF_NAMED_QUERY).setParameter("id", entity.getId());
        return query.executeUpdate() > 0;
    }
}
