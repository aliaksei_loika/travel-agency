package com.epam.travelagency.repository.impl.jpa;

import com.epam.travelagency.domain.Tour;
import com.epam.travelagency.repository.TourRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository("tourRepository")
@Profile("jpa")
public class HibernateTourRepository implements TourRepository {

    private static final String HQL_TOUR_DELETE_QUERY = "DELETE Tour t WHERE t.id = :id";

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public List<Tour> takeAll() {
        CriteriaQuery<Tour> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(Tour.class);
        Root<Tour> root = criteriaQuery.from(Tour.class);
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean create(Tour entity) {
        entityManager.joinTransaction();
        entityManager.persist(entity);
        return true;
    }

    @Override
    public Optional<Tour> take(long id) {
        return Optional.ofNullable(entityManager.find(Tour.class, id));
    }

    @Override
    public boolean update(Tour entity) {
        entityManager.joinTransaction();
        return entityManager.merge(entity) != null;
    }

    @Override
    public boolean delete(Tour entity) {
        entityManager.joinTransaction();
        Query query = entityManager.createQuery(HQL_TOUR_DELETE_QUERY);
        query.setParameter("id", entity.getId());
        return query.executeUpdate() > 0;
    }
}
