package com.epam.travelagency.repository.impl.postgres.rowmapper;

import com.epam.travelagency.domain.Country;
import com.epam.travelagency.domain.Hotel;
import com.epam.travelagency.domain.Tour;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component("tourRowMapper")
public class TourRowMapper implements RowMapper<Tour> {
    @Override
    public Tour mapRow(ResultSet resultSet, int i) throws SQLException {
        Tour tour = new Tour();

        tour.setId(resultSet.getLong("id"));
        tour.setPhoto(resultSet.getBytes("photo"));
        tour.setDate(resultSet.getDate("start_date").toLocalDate());
        tour.setDuration(resultSet.getInt("duration"));
        tour.setDescription(resultSet.getString("description"));
        tour.setCost(resultSet.getInt("cost"));

        Hotel hotel = new Hotel();
        hotel.setId(resultSet.getLong("id"));

        Tour.TourType tourType = Tour.getTourType(resultSet.getLong("tourtype_id"));

        Country country = new Country();
        country.setId(resultSet.getLong("country_id"));

        tour.setHotel(hotel);
        tour.setTourType(tourType);
        tour.setCountry(country);
        return tour;
    }
}
