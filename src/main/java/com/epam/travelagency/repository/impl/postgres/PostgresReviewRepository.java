package com.epam.travelagency.repository.impl.postgres;

import com.epam.travelagency.domain.Review;
import com.epam.travelagency.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository("reviewRepository")
@Profile("postgresql")
public class PostgresReviewRepository implements ReviewRepository {
    private static final String SQL_TAKE_ALL_REVIEWS = "SELECT * FROM review";
    private static final String SQL_CREATE_NEW_REVIEW = "INSERT INTO review(content, tour_id, user_id) VALUES (?, ?, ?)";
    private static final String SQL_TAKE_REVIEW_BY_ID = "SELECT * FROM review WHERE id = ?";
    private static final String SQL_UPDATE_REVIEW = "UPDATE review SET content=?, tour_id=?, user_id=? WHERE id=?";
    private static final String SQL_DELETE_REVIEW = "DELETE FROM review WHERE id=?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("reviewRowMapper")
    private RowMapper<Review> rowMapper;

    public PostgresReviewRepository(JdbcTemplate jdbcTemplate, RowMapper<Review> rowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = rowMapper;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setRowMapper(RowMapper<Review> rowMapper) {
        this.rowMapper = rowMapper;
    }

    @Override
    public List<Review> takeAll() {
        return jdbcTemplate.query(SQL_TAKE_ALL_REVIEWS, rowMapper);
    }

    @Override
    public boolean create(Review entity) {
        return jdbcTemplate.update(SQL_CREATE_NEW_REVIEW, entity.getContent(), entity.getTour().getId(), entity.getUser().getId()) > 0;
    }

    @Override
    public Optional<Review> take(long id) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_TAKE_REVIEW_BY_ID, new Object[]{id}, rowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean update(Review entity) {
        return jdbcTemplate.update(SQL_UPDATE_REVIEW, entity.getContent(), entity.getTour().getId(), entity.getUser().getId(), entity.getId()) > 0;
    }

    @Override
    public boolean delete(Review entity) {
        return jdbcTemplate.update(SQL_DELETE_REVIEW, entity.getId()) > 0;
    }


}
