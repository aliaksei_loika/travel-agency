package com.epam.travelagency.repository.impl.postgres;

import com.epam.travelagency.domain.User;
import com.epam.travelagency.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository("userRepository")
@Profile("postgresql")
public class PostgresUserRepository implements UserRepository {
    private static final String SQL_TAKE_ALL_USERS = "SELECT * FROM users";
    private static final String SQL_CREATE_NEW_USER = "INSERT INTO users(login, pass) VALUES (?, ?)";
    private static final String SQL_TAKE_USER_BY_ID = "SELECT * FROM users WHERE id = ?";
    private static final String SQL_UPDATE_USER = "UPDATE users SET login=?, pass=? WHERE id=?";
    private static final String SQL_DELETE_USER = "DELETE FROM users WHERE id=?";
    
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("userRowMapper")
    private RowMapper<User> rowMapper;

    public PostgresUserRepository(JdbcTemplate jdbcTemplate, RowMapper<User> rowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.rowMapper = rowMapper;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setRowMapper(RowMapper<User> rowMapper) {
        this.rowMapper = rowMapper;
    }

    @Override
    public List<User> takeAll() {
        return jdbcTemplate.query(SQL_TAKE_ALL_USERS, rowMapper);
    }

    @Override
    public boolean create(User entity) {
        return jdbcTemplate.update(SQL_CREATE_NEW_USER, entity.getLogin(), entity.getPassword()) > 0;
    }

    @Override
    public Optional<User> take(long id) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_TAKE_USER_BY_ID, new Object[]{id}, rowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean update(User entity) {
        return jdbcTemplate.update(SQL_UPDATE_USER, entity.getLogin(), entity.getPassword(), entity.getId()) > 0;
    }

    @Override
    public boolean delete(User entity) {
        return jdbcTemplate.update(SQL_DELETE_USER, entity.getId()) > 0;
    }
}
