package com.epam.travelagency.repository.impl.jpa;

import com.epam.travelagency.domain.Country;
import com.epam.travelagency.repository.CountryRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository("countryRepository")
@Profile("jpa")
public class HibernateCountryRepository implements CountryRepository {

    private static final String NATIVE_COUNTRY_UPDATE_QUERY = "UPDATE country SET name = ? WHERE id = ?";
    private static final String NAME_OF_NAMED_QUERY = "deleteCountryById";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Country> takeAll() {
        CriteriaQuery<Country> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(Country.class);
        Root<Country> root = criteriaQuery.from(Country.class);
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean create(Country entity) {
        entityManager.joinTransaction();
        entityManager.persist(entity);
        return true;
    }

    @Override
    public Optional<Country> take(long id) {
        return Optional.ofNullable(entityManager.find(Country.class, id));
    }

    @Override
    public boolean update(Country entity) {
        entityManager.joinTransaction();
        Query query = entityManager.createNativeQuery(NATIVE_COUNTRY_UPDATE_QUERY, Country.class);
        query.setParameter(1, entity.getName());
        query.setParameter(2, entity.getId());
        return query.executeUpdate() > 0;
    }

    @Override
    public boolean delete(Country entity) {
        entityManager.joinTransaction();
        Query query = entityManager.createNamedQuery(NAME_OF_NAMED_QUERY).setParameter("id", entity.getId());
        return query.executeUpdate() > 0;
    }
}
