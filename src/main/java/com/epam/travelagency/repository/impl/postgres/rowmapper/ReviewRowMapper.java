package com.epam.travelagency.repository.impl.postgres.rowmapper;

import com.epam.travelagency.domain.Review;
import com.epam.travelagency.domain.Tour;
import com.epam.travelagency.domain.User;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;


@Component("reviewRowMapper")
public class ReviewRowMapper implements RowMapper<Review> {
    @Override
    public Review mapRow(ResultSet resultSet, int i) throws SQLException {
        Review review = new Review();

        review.setId(resultSet.getLong("id"));
        review.setContent(resultSet.getString("content"));

        Tour tour = new Tour();
        tour.setId(resultSet.getLong("tour_id"));
        review.setTour(tour);

        User user = new User();
        user.setId(resultSet.getLong("user_id"));
        review.setUser(user);

        return review;
    }
}
