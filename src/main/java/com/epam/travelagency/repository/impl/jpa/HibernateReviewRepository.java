package com.epam.travelagency.repository.impl.jpa;

import com.epam.travelagency.domain.Review;
import com.epam.travelagency.repository.ReviewRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository("reviewRepository")
@Profile("jpa")
public class HibernateReviewRepository implements ReviewRepository {

    private static final String NATIVE_REVIEW_UPDATE_QUERY = "UPDATE review SET content = ?, tour_id = ?, user_id = ? " +
            "WHERE id = ?";
    private static final String NAME_OF_NAMED_QUERY = "deleteReviewById";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Review> takeAll() {
        CriteriaQuery<Review> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(Review.class);
        Root<Review> root = criteriaQuery.from(Review.class);
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean create(Review entity) {
        entityManager.joinTransaction();
        entityManager.persist(entity);
        return true;
    }

    @Override
    public Optional<Review> take(long id) {
        return Optional.ofNullable(entityManager.find(Review.class, id));
    }

    @Override
    public boolean update(Review entity) {
        Query query = entityManager.createNativeQuery(NATIVE_REVIEW_UPDATE_QUERY, Review.class);
        query.setParameter(1, entity.getContent());
        query.setParameter(2, entity.getTour().getId());
        query.setParameter(3, entity.getUser().getId());
        query.setParameter(4, entity.getId());
        return query.executeUpdate() > 0;
    }

    @Override
    public boolean delete(Review entity) {
        entityManager.joinTransaction();
        Query query = entityManager.createNamedQuery(NAME_OF_NAMED_QUERY).setParameter("id", entity.getId());
        return query.executeUpdate() > 0;
    }
}
