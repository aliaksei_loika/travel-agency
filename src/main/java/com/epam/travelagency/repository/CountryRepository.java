package com.epam.travelagency.repository;

import com.epam.travelagency.domain.Country;

public interface CountryRepository  extends Repository<Country>  {
}
