package com.epam.travelagency.repository;

import com.epam.travelagency.domain.Hotel;

public interface HotelRepository extends Repository<Hotel> {
}
