package com.epam.travelagency.repository;

import com.epam.travelagency.domain.Tour;

public interface TourRepository extends Repository<Tour> {
}
