package com.epam.travelagency.aspect;


import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RepositoryLogger {

    private static final Logger LOGGER = LogManager.getLogger();

    private static final String START_MSG = "The repository method is started";
    private static final String END_MSG = "The repository method has finished";

    @Pointcut("execution(* com.epam.travelagency.repository.*.*(..))")
    public void repositoryMethod() {
    }

    @Before("repositoryMethod()")
    public void logAtTheBeginningOfMethod() {
        LOGGER.log(Level.INFO, START_MSG);
    }

    @AfterReturning("repositoryMethod()")
    public void logAtTheEndOfMethod() {
        LOGGER.log(Level.INFO, END_MSG);
    }


}
