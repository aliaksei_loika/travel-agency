package com.epam.travelagency.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * The Class Hotel.
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@javax.persistence.Entity
@NamedQuery(name = "deleteHotelById", query = "DELETE FROM Hotel h WHERE h.id = :id")
public class Hotel extends Entity {

    /**
     * The name.
     */
    @NotNull
    @Size(max = 100)
    private String name;

    /**
     * The phone.
     */
    @Size(max = 30)
    private String phone;

    /**
     * The country.
     */
    @NotNull
    @ManyToOne
    private Country country;

    /**
     * The stars.
     */
    @NotNull
    @Min(1)
    @Max(5)
    private int stars;

}
