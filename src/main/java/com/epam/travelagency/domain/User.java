package com.epam.travelagency.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

/**
 * The Class User.
 */
@Data
@EqualsAndHashCode(callSuper = true, exclude = {"reviews", "tours"})
@ToString(exclude = {"password"}, callSuper = true)
@javax.persistence.Entity
@Table(name = "users")
@NamedQuery(name = "deleteUserById", query = "DELETE FROM User u WHERE u.id = :id")
public class User extends Entity {

    /**
     * The login.
     */
    @NotNull
    @Size(max = 30)
    private String login;

    /**
     * The password.
     */
    @NotNull
    @Size(max = 30)
    @Column(name = "pass")
    private String password;

    /**
     * The tours.
     */
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "tour_user",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "tour_id"))
    private List<Tour> tours;

    /**
     * The reviews.
     */
//    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    private List<Review> reviews;

}
