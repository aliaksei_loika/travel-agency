package com.epam.travelagency.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * The Class Review.
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@javax.persistence.Entity
@NamedQuery(name = "deleteReviewById", query = "DELETE FROM Review r WHERE r.id = :id")
public class Review extends Entity {

    /**
     * The tour.
     */
    @ManyToOne
    private Tour tour;

    /**
     * The user.
     */
    @ManyToOne
    private User user;

    /**
     * The content.
     */
    @NotNull
    @Size(max = 1000)
    private String content;
}
