package com.epam.travelagency.domain;

import com.epam.travelagency.util.TourTypeConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * The Class Tour.
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@javax.persistence.Entity
@Table(name = "tour")
public class Tour extends Entity {

    /**
     * The photo.
     */
    @Column(name = "photo")
    private byte[] photo; // SerialBlob ??

    /**
     * The date.
     */
    @NotNull
    @Column(name = "start_date")
    private LocalDate date;

    /**
     * The duration.
     */
    @Column(name = "duration")
    private int duration;

    /**
     * The country.
     */
    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    /**
     * The hotel.
     */
    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;

    /**
     * The tour type.
     */
    @Convert(converter = TourTypeConverter.class)
    @Column(name = "tourtype_id")
    private TourType tourType;

    /**
     * The description.
     */
    @Size(max = 1000)
    @Column(name = "description")
    private String description;

    /**
     * The cost.
     */
    @Column(name = "cost")
    private int cost;

    /**
     * The Enum TourType.
     */
    public enum TourType {
        /**
         * The normal.
         */
        NORMAL(1L),
        /**
         * The hot.
         */
        HOT(2L),
        /**
         * The pre order.
         */
        PRE_ORDER(3L);

        private Long id;

        TourType(Long id) {
            this.id = id;
        }

        public Long getId() {
            return id;
        }
    }

    public static TourType getTourType(Long tourTypeId) {

        for (TourType type : TourType.values()) {
            if (type.ordinal() + 1 == tourTypeId) {
                return type;
            }
        }

        return TourType.NORMAL;
    }

}
