package com.epam.travelagency.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * The Class Country.
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@javax.persistence.Entity
@Table(name = "country")
@NamedQuery(name = "deleteCountryById", query = "DELETE FROM Country c WHERE c.id = :id")
public class Country extends Entity {

    /**
     * The name.
     */
    @Column(name = "name")
    @Size(max = 100)
    private String name;


}
