package com.epam.travelagency.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;

import javax.persistence.*;

/**
 * The Abstract Class Entity.
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(exclude = "version")
@MappedSuperclass
@OptimisticLocking(type = OptimisticLockType.VERSION)
public abstract class Entity {

    /**
     * The id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Version
    private int version;


    public Entity(long id) {
        this.id = id;
    }

}
