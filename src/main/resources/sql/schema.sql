CREATE SCHEMA "public";

CREATE TABLE "public".country (
  id   serial NOT NULL,
  name varchar(100),
  CONSTRAINT pk_country_id PRIMARY KEY (id)
);

CREATE TABLE "public".hotel (
  id         serial       NOT NULL,
  name       varchar(100) NOT NULL,
  phone      varchar(100) NOT NULL,
  stars      integer      NOT NULL,
  country_id integer      NOT NULL,
  CONSTRAINT pk_tour_id PRIMARY KEY (id)
);

CREATE TABLE "public".tourtype (
  id   serial NOT NULL,
  name varchar(30),
  CONSTRAINT pk_tourtype_id PRIMARY KEY (id)
);

CREATE TABLE "public".users (
  id    serial      NOT NULL,
  login varchar(30) NOT NULL,
  pass  varchar(30) NOT NULL,
  CONSTRAINT pk_user_id PRIMARY KEY (id)
);

CREATE TABLE "public".tour (
  id          serial  NOT NULL,
  photo       bytea,
  start_date  date    NOT NULL,
  duration    integer,
  hotel_id    integer,
  tourtype_id integer,
  country_id  integer,
  description text,
  cost        integer NOT NULL,
  CONSTRAINT pk_tour_id_0 PRIMARY KEY (id)
);

CREATE INDEX idx_tour_tourtype_id
  ON "public".tour (tourtype_id);

CREATE INDEX idx_tour_country_id
  ON "public".tour (country_id);

CREATE TABLE "public".tour_user (
  tour_id integer NOT NULL,
  user_id integer NOT NULL
);

CREATE INDEX idx_tour_user_tour_id
  ON "public".tour_user (tour_id);

CREATE INDEX idx_tour_user_user_id
  ON "public".tour_user (user_id);

CREATE TABLE "public".review (
  id      serial NOT NULL,
  content text   NOT NULL,
  tour_id integer,
  user_id integer,
  CONSTRAINT pk_review_id PRIMARY KEY (id)
);

CREATE INDEX idx_review_tour_id
  ON "public".review (tour_id);

CREATE INDEX idx_review_user_id
  ON "public".review (user_id);

ALTER TABLE "public".hotel
  ADD CONSTRAINT fk_hotel_country FOREIGN KEY (country_id) REFERENCES "public".country (id);

ALTER TABLE "public".review
  ADD CONSTRAINT fk_review_tour FOREIGN KEY (tour_id) REFERENCES "public".tour (id);

ALTER TABLE "public".review
  ADD CONSTRAINT fk_review_user FOREIGN KEY (user_id) REFERENCES "public".users (id);

ALTER TABLE "public".tour
  ADD CONSTRAINT fk_tour_hotel FOREIGN KEY (hotel_id) REFERENCES "public".hotel (id);

ALTER TABLE "public".tour
  ADD CONSTRAINT fk_tour_tourtype FOREIGN KEY (tourtype_id) REFERENCES "public".tourtype (id);

ALTER TABLE "public".tour
  ADD CONSTRAINT fk_tour_country FOREIGN KEY (country_id) REFERENCES "public".country (id);

ALTER TABLE "public".tour_user
  ADD CONSTRAINT fk_tour_user_tour FOREIGN KEY (tour_id) REFERENCES "public".tour (id);

ALTER TABLE "public".tour_user
  ADD CONSTRAINT fk_tour_user_user FOREIGN KEY (user_id) REFERENCES "public".users (id);

