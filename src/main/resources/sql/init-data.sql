INSERT INTO "public".country (id, name) VALUES (1, 'Belarus');
INSERT INTO "public".country (id, name) VALUES (3, 'Thailand');
INSERT INTO "public".country (id, name) VALUES (2, 'Egypt');

INSERT INTO "public".hotel (id, name, phone, stars, country_id) VALUES (2, 'U liasnika resort', '+375297773355', 2, 1);
INSERT INTO "public".hotel (id, name, phone, stars, country_id) VALUES (3, 'Fulla place', '+79508882233', 3, 3);
INSERT INTO "public".hotel (id, name, phone, stars, country_id) VALUES (4, 'Coral Hotel', '+1600100700', 5, 2);
INSERT INTO "public".hotel (id, name, phone, stars, country_id) VALUES (5, 'Phi-Phi lagoon', '+1611100799', 4, 3);

INSERT INTO "public".tourtype (id, name) VALUES (1, 'normal');
INSERT INTO "public".tourtype (id, name) VALUES (2, 'hot');
INSERT INTO "public".tourtype (id, name) VALUES (3, 'pre-order');

INSERT INTO "public".users (id, login, pass) VALUES (1, 'arkady_dobkin', '123');
INSERT INTO "public".users (id, login, pass) VALUES (2, 'fedor_emelianenko', '111');
INSERT INTO "public".users (id, login, pass) VALUES (3, 'ihar_blinou', 'qwerty');
INSERT INTO "public".users (id, login, pass) VALUES (4, 'tester', 'test');

INSERT INTO "public".tour (id, photo, start_date, duration, hotel_id, tourtype_id, country_id, description, cost)
VALUES (2, NULL, '2018-01-20', 14, 4, 1, 2, 'good choice for family', 1200);
INSERT INTO "public".tour (id, photo, start_date, duration, hotel_id, tourtype_id, country_id, description, cost)
VALUES (3, NULL, '2018-03-17', 7, 2, 3, 1, 'if you don''t have the money', 200);
INSERT INTO "public".tour (id, photo, start_date, duration, hotel_id, tourtype_id, country_id, description, cost)
VALUES (4, NULL, '2018-02-02', 21, 3, 2, 3, 'good hot sea', 800);

INSERT INTO "public".review (id, content, tour_id, user_id) VALUES (1, 'goog hotel', 2, 1);
INSERT INTO "public".review (id, content, tour_id, user_id) VALUES (2, 'if you like extreme', 3, 3);
INSERT INTO "public".review (id, content, tour_id, user_id) VALUES (3, 'very cool', 2, 2);

